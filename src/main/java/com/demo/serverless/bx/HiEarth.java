package com.demo.serverless.bx;

import com.google.gson.JsonObject;

public class HiEarth {
    public static JsonObject main(JsonObject args) {
        String name = "stranger";
        if (args.has("name"))
            name = args.getAsJsonPrimitive("name").getAsString();
        JsonObject response = new JsonObject();
        response.addProperty("greeting", "Hi " + name + "!");
        return response;
    }
}